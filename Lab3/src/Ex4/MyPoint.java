package Ex4;

public class MyPoint {
    int x,y;
    MyPoint(){
        this.x=0;
        this.y=0;
    }
    MyPoint(int a, int b){
        this.x=a;
        this.y=b;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
    public void setXY(int x, int y){
        this.x=x;
        this.y=y;
    }
    public String toString() {
        String c = "(" + Integer.toString(this.x) + ", " + Integer.toString(this.y) + ")";
        return c;
    }
    public double distance(int x,int y){
        return Math.sqrt((this.x-x)*(this.x-x)+(this.y-y)*(this.y-y));
    }
    public double distance(MyPoint m) {
        return Math.sqrt((this.x - m.x) * (this.x - m.x) + (this.y - m.y) * (this.y - m.y));
    }
}