package Ex2;

public class Circle {
    private double radius;
    private String color;
    public Circle(){
        this.radius=1.0;
        this.color="red";
    }
    Circle(double r){
        radius=r;
        color="noColor";
    }
    Circle(double r, String c){
        radius=r;
        color=c;
    }
    public double getRadius() {
        return radius;
    }
    public double getArea(){
        double area=2*Math.PI*radius;
        return area;
    }
    public void getColor(String c) {
        this.color = c;
    }

}


