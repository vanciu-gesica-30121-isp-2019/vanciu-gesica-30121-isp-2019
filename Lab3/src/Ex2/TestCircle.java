package Ex2;

import static java.lang.System.out;

public class TestCircle {
    public static void main(String args[]) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(2.0);
        Circle c3 = new Circle(4.0,"blue");
        out.println(c1.getRadius());
        out.println(c1.getArea());
        out.println(c2.getRadius());
        out.println(c2.getArea());
        out.println(c3.getRadius());
        out.println(c3.getArea());
    }
}

