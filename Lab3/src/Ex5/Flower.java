package Ex5;

public class Flower {
    static int var = 0;

    Flower() {
        System.out.println("Flower has been created!");
        var++;
    }

    public static int obc() {
        return var;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[11];
        for (int i = 0; i < 10; i++) {
            Flower f = new Flower();
            garden[i] = f;
        }
        System.out.println("Numarul de obiecte create: " + obc());
    }
}