package Ex2;

import javax.swing.*;

public class Incr extends JFrame {

    private int counter = 0;

    public Incr() {
        super("Press_Button_Increment_Value");
        setSize(150, 150);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initComponents();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void initComponents() {
        JPanel layout = new JPanel();
        JLabel counterLbl = new JLabel(Integer.toString(counter));
        layout.add(counterLbl);

        JButton incButton = new JButton("Increment value");
        incButton.addActionListener(l -> {
            counterLbl.setText(Integer.toString(++counter));
        });
        layout.add(incButton);

        add(layout);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new Incr());
    }
}