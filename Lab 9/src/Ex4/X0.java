package Ex4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class X0 extends JFrame{


    int jucator=0;
    int j1=0,j2=0;
    int[][] matrice = new int[4][4];

    JButton a11,a12,a13,a21,a22,a23,a31,a32,a33;
    JTextField text1,scor1,scor2;
    JLabel g1,g2;

    X0(){


        setTitle("Tic Tac Toe");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,450);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int width=100;
        int height = 100;



        a11 = new JButton("");
        a11.setBounds(25,25,width, height);
        a11.addActionListener(new TratareButonLoghin());
        a11.setActionCommand("11");

        a12 = new JButton("");
        a12.setBounds(135,25,width, height);
        a12.addActionListener(new TratareButonLoghin());
        a12.setActionCommand("12");

        a13 = new JButton("");
        a13.setBounds(245,25,width, height);
        a13.addActionListener(new TratareButonLoghin());
        a13.setActionCommand("13");

        a21 = new JButton("");
        a21.setBounds(25,135,width, height);
        a21.addActionListener(new TratareButonLoghin());
        a21.setActionCommand("21");

        a22 = new JButton("");
        a22.setBounds(135,135,width, height);
        a22.addActionListener(new TratareButonLoghin());
        a22.setActionCommand("22");

        a23 = new JButton("");
        a23.setBounds(245,135,width, height);
        a23.addActionListener(new TratareButonLoghin());
        a23.setActionCommand("23");

        a31 = new JButton("");
        a31.setBounds(25,245,width, height);
        a31.addActionListener(new TratareButonLoghin());
        a31.setActionCommand("31");

        a32 = new JButton("");
        a32.setBounds(135,245,width, height);
        a32.addActionListener(new TratareButonLoghin());
        a32.setActionCommand("32");

        a33 = new JButton("");
        a33.setBounds(245,245,width, height);
        a33.addActionListener(new TratareButonLoghin());
        a33.setActionCommand("33");


        text1 = new JTextField ();
        text1.setBounds (135,350,150,25);
        text1.setEditable (false);
        add(text1);
        scor1 = new JTextField ();
        scor1.setBounds (45,350,20,25);
        scor1.setEditable (false);
        add (scor1);
        scor2 = new JTextField ();
        scor2.setBounds (45,377,20,25);
        scor2.setEditable (false);
        add (scor2);
        g1 = new JLabel ("Scor 0:");
        g1.setBounds (0,360,60,60);
        add (g1);
        g2 = new JLabel ("Scor X:");
        g2.setBounds (0,333,60,60);
        add (g2);
        scor1.setText ("0");
        scor2.setText ("0");

        add(a11); add(a12); add(a13);
        add(a21); add(a22); add(a23);
        add(a31); add(a32); add(a33);


    }

    public void reinit() {
        a11.setText("");
        a11.setEnabled(true);
        a12.setText("");
        a12.setEnabled(true);
        a13.setText("");
        a13.setEnabled(true);
        a21.setText("");
        a21.setEnabled(true);
        a22.setText("");
        a22.setEnabled(true);
        a23.setText("");
        a23.setEnabled(true);
        a31.setText("");
        a31.setEnabled(true);
        a32.setText("");
        a32.setEnabled(true);
        a33.setText("");
        a33.setEnabled(true);
        int i,j;
        for(i=1;i<=3;i++)
            for(j=1;j<=3;j++)
                matrice[i][j]=0;

    }

    public static void main(String[] args) {
        X0 f = new X0();
    }

    class TratareButonLoghin implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            String s = e.getActionCommand ();
            if ("11".equals (s)) {
                X0.this.a11.setEnabled (false);
                if (jucator % 2 == 0) {
                    X0.this.a11.setText ("X");
                    matrice[1][1] = 1;
                    if ((matrice[1][1] == matrice[1][2] && matrice[1][1] == matrice[1][3]) || (matrice[1][1] == matrice[2][1] && matrice[1][1] == matrice[3][1]) || (matrice[1][1] == matrice[2][2] && matrice[1][1] == matrice[3][3])) {
                        reinit ();
                        j1++;
                        text1.setText ("X castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);

                    }
                } else {
                    X0.this.a11.setText ("O");
                    matrice[1][1] = 2;
                    if ((matrice[1][1] == matrice[1][2] && matrice[1][1] == matrice[1][3]) || (matrice[1][1] == matrice[2][1] && matrice[1][1] == matrice[3][1]) || (matrice[1][1] == matrice[2][2] && matrice[1][1] == matrice[3][3])) {
                        reinit ();
                        j2++;
                        text1.setText ("O castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                }
                jucator = jucator + 1;
            } else if ("12".equals (s)) {
                X0.this.a12.setEnabled (false);
                if (jucator % 2 == 0) {
                    X0.this.a12.setText ("X");
                    matrice[1][2] = 1;
                    if ((matrice[1][2] == matrice[1][1] && matrice[1][2] == matrice[1][3]) || (matrice[1][2] == matrice[2][2] && matrice[1][2] == matrice[3][2])) {
                        reinit ();
                        j1++;
                        text1.setText ("X castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);

                    }
                } else {
                    X0.this.a12.setText ("O");
                    matrice[1][2] = 2;
                    if ((matrice[1][2] == matrice[1][1] && matrice[1][2] == matrice[1][3]) || (matrice[1][2] == matrice[2][2] && matrice[1][2] == matrice[3][2])) {
                        reinit ();
                        j2++;
                        text1.setText ("O castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                }
                jucator = jucator + 1;
            } else if ("13".equals (s)) {
                X0.this.a13.setEnabled (false);
                if (jucator % 2 == 0) {
                    X0.this.a13.setText ("X");
                    matrice[1][3] = 1;
                    if ((matrice[1][3] == matrice[1][1] && matrice[1][3] == matrice[1][2]) || (matrice[1][3] == matrice[2][3] && matrice[1][3] == matrice[3][3]) || (matrice[1][3] == matrice[2][2] && matrice[1][3] == matrice[3][1])) {
                        reinit ();
                        j1++;
                        text1.setText ("X castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                } else {
                    X0.this.a13.setText ("O");
                    matrice[1][3] = 2;
                    if ((matrice[1][3] == matrice[1][1] && matrice[1][3] == matrice[1][2]) || (matrice[1][3] == matrice[2][3] && matrice[1][3] == matrice[3][3]) || (matrice[1][3] == matrice[2][2] && matrice[1][3] == matrice[3][1])) {
                        reinit ();
                        j2++;
                        text1.setText ("O castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                }
                jucator = jucator + 1;
            } else if ("21".equals (s)) {
                X0.this.a21.setEnabled (false);
                if (jucator % 2 == 0) {
                    X0.this.a21.setText ("X");
                    matrice[2][1] = 1;
                    if ((matrice[2][1] == matrice[1][1] && matrice[2][1] == matrice[3][1]) || (matrice[2][1] == matrice[2][2] && matrice[2][1] == matrice[2][3])) {
                        reinit ();
                        j1++;
                        text1.setText ("X castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                } else {
                    X0.this.a21.setText ("O");
                    matrice[2][1] = 2;
                    if ((matrice[2][1] == matrice[1][1] && matrice[2][1] == matrice[3][1]) || (matrice[2][1] == matrice[2][2] && matrice[2][1] == matrice[2][3])) {
                        reinit ();
                        j2++;
                        text1.setText ("O castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                }
                jucator = jucator + 1;
            } else if ("22".equals (s)) {
                X0.this.a22.setEnabled (false);
                if (jucator % 2 == 0) {
                    X0.this.a22.setText ("X");
                    matrice[2][2] = 1;
                    if ((matrice[2][2] == matrice[1][1] && matrice[2][2] == matrice[3][3]) || (matrice[2][2] == matrice[3][1] && matrice[2][2] == matrice[1][3]) || (matrice[2][2] == matrice[2][1] && matrice[2][2] == matrice[2][3]) || (matrice[2][2] == matrice[1][2] && matrice[2][2] == matrice[3][2])) {
                        reinit ();
                        j1++;
                        text1.setText ("X castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                } else {
                    X0.this.a22.setText ("O");
                    matrice[2][2] = 2;
                    if ((matrice[2][2] == matrice[1][1] && matrice[2][2] == matrice[3][3]) || (matrice[2][2] == matrice[3][1] && matrice[2][2] == matrice[1][3]) || (matrice[2][2] == matrice[2][1] && matrice[2][2] == matrice[2][3]) || (matrice[2][2] == matrice[1][2] && matrice[2][2] == matrice[3][2])) {
                        reinit ();
                        j2++;
                        text1.setText ("O castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                }
                jucator = jucator + 1;
            } else if ("23".equals (s)) {
                X0.this.a23.setEnabled (false);
                if (jucator % 2 == 0) {
                    X0.this.a23.setText ("X");
                    matrice[2][3] = 1;
                    if ((matrice[2][3] == matrice[2][2] && matrice[2][3] == matrice[2][1]) || (matrice[2][3] == matrice[1][3] && matrice[2][3] == matrice[3][3])) {
                        reinit ();
                        j1++;
                        text1.setText ("X castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                } else {
                    X0.this.a23.setText ("O");
                    matrice[2][3] = 2;
                    if ((matrice[2][3] == matrice[2][2] && matrice[2][3] == matrice[2][1]) || (matrice[2][3] == matrice[1][3] && matrice[2][3] == matrice[3][3])) {
                        reinit ();
                        j2++;
                        text1.setText ("O castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                }
                jucator = jucator + 1;
            } else if ("31".equals (s)) {
                X0.this.a31.setEnabled (false);
                if (jucator % 2 == 0) {
                    X0.this.a31.setText ("X");
                    matrice[3][1] = 1;
                    if ((matrice[3][1] == matrice[2][1] && matrice[3][1] == matrice[1][1]) || (matrice[3][1] == matrice[2][2] && matrice[3][1] == matrice[1][3]) || (matrice[3][1] == matrice[3][2] && matrice[3][1] == matrice[3][3])) {
                        reinit ();
                        j1++;
                        text1.setText ("X castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                } else {
                    X0.this.a31.setText ("O");
                    matrice[3][1] = 2;
                    if ((matrice[3][1] == matrice[2][1] && matrice[3][1] == matrice[1][1]) || (matrice[3][1] == matrice[2][2] && matrice[3][1] == matrice[1][3]) || (matrice[3][1] == matrice[3][2] && matrice[3][1] == matrice[3][3])) {
                        reinit ();
                        j2++;
                        text1.setText ("O castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                }
                jucator = jucator + 1;
            } else if ("32".equals (s)) {
                X0.this.a32.setEnabled (false);
                if (jucator % 2 == 0) {
                    X0.this.a32.setText ("X");
                    matrice[3][2] = 1;
                    if ((matrice[3][2] == matrice[3][1] && matrice[3][2] == matrice[3][3]) || (matrice[3][2] == matrice[2][2] && matrice[3][2] == matrice[1][2])) {
                        reinit ();
                        j1++;
                        text1.setText ("X castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                } else {
                    X0.this.a32.setText ("O");
                    matrice[3][2] = 2;
                    if ((matrice[3][2] == matrice[3][1] && matrice[3][2] == matrice[3][3]) || (matrice[3][2] == matrice[2][2] && matrice[3][2] == matrice[1][2])) {
                        reinit ();
                        j2++;
                        text1.setText ("O castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                }
                jucator = jucator + 1;
            } else if ("33".equals (s)) {
                X0.this.a33.setEnabled (false);
                if (jucator % 2 == 0) {
                   X0.this.a33.setText ("X");
                    matrice[3][3] = 1;
                    if ((matrice[3][3] == matrice[2][2] && matrice[3][3] == matrice[1][1]) || (matrice[3][3] == matrice[3][2] && matrice[3][3] == matrice[3][1]) || (matrice[3][3] == matrice[2][3] && matrice[3][3] == matrice[1][3])) {
                        reinit ();
                        j1++;
                        text1.setText ("X castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);
                    }
                } else {
                    X0.this.a33.setText ("O");
                    matrice[3][3] = 2;
                    if ((matrice[3][3] == matrice[2][2] && matrice[3][3] == matrice[1][1]) || (matrice[3][3] == matrice[3][2] && matrice[3][3] == matrice[3][1]) || (matrice[3][3] == matrice[2][3] && matrice[3][3] == matrice[1][3])) {
                        reinit ();
                        j2++;
                        text1.setText ("O castiga - scor " + j1 + "-" + j2);
                        scor1.setText (""+j1);
                        scor2.setText (""+j2);

                    }
                }
                jucator = jucator + 1;
            }

            int i,j,k=0;
            for(i=1;i<=3;i++)
                for(j=1;j<=3;j++)
                    if(matrice[i][j]!=0)
                        k++;
            if(k==9) {
                reinit();
                text1.setText ("Egalitate - scor "+j1+"-"+j2);
            }


        }

    }
}



