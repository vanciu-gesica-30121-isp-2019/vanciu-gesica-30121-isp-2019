package Ex3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class WriteInFile {
    public void write () throws Exception {
        try {
            File myFile = new File ("data3.txt");
            PrintWriter scriere = new PrintWriter ("data3.txt");
            Scanner citire = new Scanner (System.in);
            System.out.println ("Dati textul pe care vreti sa-l criptati: ");
            String l = citire.nextLine ();
            scriere.print (l);
            scriere.close ();
        } catch (FileNotFoundException e) {
            System.out.println ("Invalid");
        }
    }
}
