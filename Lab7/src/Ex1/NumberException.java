package Ex1;

class NumberException extends Exception {
    int n;

    public NumberException (int nr, String msg) {
        super (msg);
        this.n = nr;
    }

    int getNr () {
        return n;
    }
}