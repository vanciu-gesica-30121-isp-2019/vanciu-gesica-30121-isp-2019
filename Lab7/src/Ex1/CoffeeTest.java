package Ex1;

public class CoffeeTest {
    public static void main (String[] args) {
        CoffeeMaker mk = new CoffeeMaker ();
        CoffeeDrinker d = new CoffeeDrinker ();

        for (int i = 0; i < 15; i++) {
            Coffee c = mk.makeCoffe ();
            try {
                d.drinkCoffe (c);
            } catch (TemperatureException e) {
                System.out.println ("Exception:" + e.getMessage () + " temp=" + e.getTemp ());
            } catch (ConcentrationException e) {
                System.out.println ("Exception:" + e.getMessage () + " conc=" + e.getConc ());

            } catch (NumberException e) {
                System.out.println ("Exception:" + e.getMessage () + " num=" + e.getNr ());
                break;
            } finally {
                System.out.println ("Throw the coffe cup.\n");


            }
        }
    }
}
