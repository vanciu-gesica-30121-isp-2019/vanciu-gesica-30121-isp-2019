package Ex1;

public class CoffeeDrinker {

    void drinkCoffe (Coffee c) throws TemperatureException, ConcentrationException, NumberException {
            if (c.getNr () > 9)
                throw new NumberException (c.getNr (), "Coffe number to high!");
            if (c.getTemp () > 60)
                throw new TemperatureException (c.getTemp (), "Coffe is to hot!");
            if (c.getConc () > 50)
                throw new ConcentrationException (c.getConc (), "Coffe concentration to high!");

            System.out.println ("Drink coffe:" + c);
        }

}
