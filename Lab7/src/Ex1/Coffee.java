package Ex1;

public class Coffee {
    private int temp;
    private int conc;
    public static int nr = 0;


    Coffee (int t, int c) {
        temp = t;
        conc = c;
        nr++;
    }

    int getTemp () {
        return temp;
    }

    int getConc () {
        return conc;
    }

    int getNr () {
        return nr;
    }

    public String toString () {
        return "[coffe temperature=" + temp + ":concentration=" + conc + "numar=" + nr + "]";
    }}

