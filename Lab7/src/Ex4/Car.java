package Ex4;

import java.io.Serializable;
import java.util.Scanner;

public class Car implements Serializable {

    private String model;
    private int price;

    public Car () { }

    public Car (String model, int price) {
        this.model = model;
        this.price = price;
    }

    public String getModel () {
        return model;
    }

    public int getPrice () {
        return price;
    }

    public void setModel (String model) {
        this.model = model;
    }

    public void setPrice (int price) {
        this.price = price;
    }

    @Override
    public String toString () {
        return "Car type: " + this.model + "\nPrice: " + this.price;
    }

    public void add_Car () {

        Scanner citire = new Scanner (System.in);
        System.out.println ("Numele masinii: ");
        model = citire.nextLine ();
        System.out.println ("Pretul masinii: ");
        price = citire.nextInt ();
        Car myCar = new Car (model, price);
    }

    public String show_Car () {
        return "Car type: " + this.model + "\nPrice: " + this.price;
    }
}