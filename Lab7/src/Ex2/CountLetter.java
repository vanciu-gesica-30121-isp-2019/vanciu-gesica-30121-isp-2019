package Ex2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;

public class CountLetter {

    private Path file;
    private char character;


    public CountLetter(Path file, char character) {
        this.file = file;
        this.character = character;
    }


    public int count() {
        int count = 0;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(file)))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == character) {
                        count++;
                    }
                }

            }
        } catch (IOException x) {
            System.err.println(x);
        }
        return count;
    }
}