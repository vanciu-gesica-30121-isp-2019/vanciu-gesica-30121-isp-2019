package Ex3;

public class Test {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Adrian",100);
        bank.addAccount("Raul",90);
        bank.addAccount("Corina",80);
        bank.addAccount("Marius",70);
        bank.addAccount("Claudia",60);
        bank.addAccount("Marcel",50);

        System.out.println("Print Accounts by balance");
        bank.printAccounts();

        System.out.println("Print Accounts between limits");
        bank.printAccounts(61,89);

        System.out.println("Get Account by owner name and balance");
        bank.getAccount("Raul",90);

        System.out.println("Get All Account ");
        bank.getAllAccount();

    }
}