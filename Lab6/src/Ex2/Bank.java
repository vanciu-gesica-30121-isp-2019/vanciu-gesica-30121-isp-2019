package Ex2;

import java.util.*;


public class Bank {
    ArrayList bankAccounts = new ArrayList();

    public void addAccount(String owner, double balance) {
        BankAccount ba = new BankAccount(owner, balance);
        bankAccounts.add(ba);
    }

    public void printAccounts() {
        int sortat;
        do {
            sortat = 1;
            for (int i = 0; i < bankAccounts.size() - 1; i++) {
                BankAccount ba1 = (BankAccount) bankAccounts.get(i);
                BankAccount ba2 = (BankAccount) bankAccounts.get(i + 1);
                BankAccount ba3 = new BankAccount(" ", 0);
                if (ba1.balance > ba2.balance) {
                    ba3.owner = ba1.owner;
                    ba3.balance = ba1.balance;
                    ba1.owner = ba2.owner;
                    ba1.balance = ba2.balance;
                    ba2.owner = ba3.owner;
                    ba2.balance = ba3.balance;
                    sortat = 0;
                }
            }
        } while (sortat == 0);

        for (int i = 0; i < bankAccounts.size(); i++) {
            BankAccount ba = (BankAccount) bankAccounts.get(i);
            System.out.println(ba.owner + " " + ba.balance);
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (int i = 0; i < bankAccounts.size(); i++) {
            BankAccount ba = (BankAccount) bankAccounts.get(i);
            if (ba.balance >= minBalance && ba.balance <= maxBalance)
                System.out.println(ba.owner + " " + ba.balance);
        }
    }

    public BankAccount getAccount(String owner)
    {
        for (int i = 0; i < bankAccounts.size(); i++) {
            BankAccount ba = (BankAccount) bankAccounts.get(i);
            if (ba.owner.equals(owner))
                System.out.println(ba.owner + " " + ba.balance);
        }
        return null;
    }

    public BankAccount getAllAccount()
    {
        int sortat;
        do {
            sortat = 1;
            for (int i = 0; i < bankAccounts.size() - 1; i++) {
                BankAccount ba1 = (BankAccount) bankAccounts.get(i);
                BankAccount ba2 = (BankAccount) bankAccounts.get(i + 1);
                BankAccount ba3 = new BankAccount(" ", 0);

                if (ba1.owner.compareTo(ba2.owner)>0) {
                    ba3.owner = ba1.owner;
                    ba3.balance = ba1.balance;
                    ba1.owner = ba2.owner;
                    ba1.balance = ba2.balance;
                    ba2.owner = ba3.owner;
                    ba2.balance = ba3.balance;
                    sortat = 0;
                }
            }
        } while (sortat == 0);

        for (int i = 0; i < bankAccounts.size(); i++) {
            BankAccount ba = (BankAccount) bankAccounts.get(i);
            System.out.println(ba.owner + " " + ba.balance);
        }
        return null;
    }

}