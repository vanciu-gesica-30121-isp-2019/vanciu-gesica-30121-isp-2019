package Ex2;

public class TestBankAccount {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Andrei",200);
        bank.addAccount("Raluca",40);
        bank.addAccount("Cosmina",70);
        bank.addAccount("Matei",50);
        bank.addAccount("Carla",30);
        bank.addAccount("Marius",150);
        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between limits");
        bank.printAccounts(60,90);
        System.out.println("Get Account by owner name");
        bank.getAccount("Raluca");
        System.out.println("Get All Account order by name");
        bank.getAllAccount();

    }
}
