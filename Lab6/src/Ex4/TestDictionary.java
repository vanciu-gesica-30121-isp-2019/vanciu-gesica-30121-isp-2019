package Ex4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestDictionary {
    public static void main(String[] args) throws IOException {

        Dictionary dict = new Dictionary();
        char raspuns;
        String linie;
        String explic;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Meniu");
            System.out.println("a - Adauga cuvant");
            System.out.println("c - Cauta cuvant");
            System.out.println("w - Listeaza cuvintele");
            System.out.println("d - Listeaza definitiile");
            System.out.println("l - Listeaza dictionar");
            System.out.println("e - Iesi");

            linie = fluxIn.readLine();
            raspuns = linie.charAt(0);

            switch (raspuns) {
                case 'a': case 'A':
                    System.out.println("Introduceti cuvantul:");
                    linie = fluxIn.readLine();
                    if (linie.length() > 1) {
                        System.out.println("Introduceti definitia:");
                        explic = fluxIn.readLine();
                        dict.addWord(new Word(linie), new Definition(explic));
                    }
                    break;
                case 'c': case 'C':
                    System.out.println("Cuvant cautat:");
                    linie = fluxIn.readLine();
                    if (linie.length() > 1) {
                        Word x = new Word(linie);
                        explic = String.valueOf(dict.getDefinition(x));

                        if (explic == null)
                            System.out.println("Cuvantul nu a fost gasit!");
                        else
                            System.out.println("Definitie:" + explic);
                    }
                    break;
                case 'l': case 'L':
                    System.out.println("Afiseaza:");
                    dict.afisDictionar();
                    break;
                case 'w': case 'W':
                    System.out.println("get all words");
                    dict.getAllWords();
                    break;
                case 'd': case 'D':
                    System.out.println("get all definitions");
                    dict.getAllDefinitions();
                    break;

            }
        } while (raspuns != 'e' && raspuns != 'E');
        System.out.println("Program terminat.");



    }

}


