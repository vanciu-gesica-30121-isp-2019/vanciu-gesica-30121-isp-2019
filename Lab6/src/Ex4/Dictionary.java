package Ex4;


import static java.lang.System.*;
import java.util.HashMap;

public class Dictionary {
    HashMap dct = new HashMap();

    public void addWord(Word w, Definition d) {
        if (dct.containsKey(w))
            out.println("Modific cuvant existent!");
        else
            out.println("Adauga cuvant nou.");
        dct.put(w, d);
    }

    public Object getDefinition(Word w) {
        out.println("Cauta " + w);
        out.println(dct.containsKey(w));
        return dct.get(w);
    }

    public void afisDictionar() {
        out.println(dct);
    }

    public void getAllWords() {

        for (Object word : dct.keySet()) System.out.println(word.toString());
    }
    public void getAllDefinitions() {

        for (Object definition : dct.values()) System.out.println(definition.toString());
    }
}
