package Ex3;

import Ex2.Author;

public class TestBook {
    public static void main(String args[]){
        Author a1 = new Author("VanciuGesica","gesicavanciu@yahoo.com",'f');
        Author a2 = new Author("OnyaCristian","onyacristian@gmail.com",'m');
        Book b1 = new Book("Just me",a1,50) ;
        Book b2 = new Book("MeAndYou",a2,100,4);
        Book b3 = new Book("YouAndMe",a1,120,3);
        System.out.println(b1.getName());
        System.out.println(b1.getAuthor());
        System.out.println(b1.getPrice());
        b1.setPrice(70);
        b1.setQtyInStock(80);
        System.out.println(b1.toString());

        System.out.println(b2.getName());
        System.out.println(b2.getAuthor());
        System.out.println(b2.getPrice());
        System.out.println(b2.getQtyInStock());
        b2.setPrice(160);
        System.out.println(b2.toString());

        System.out.println(b3.getName());
        System.out.println(b3.getAuthor());
        System.out.println(b3.getPrice());
        System.out.println(b3.getQtyInStock());
        b3.setQtyInStock(30);
        System.out.println(b3.toString());


    }
}
