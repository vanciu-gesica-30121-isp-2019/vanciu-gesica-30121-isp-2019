package Ex5;

import Ex1.Circle;

public class Cylinder extends Circle {
    private double height;
    private double volume;
    public Cylinder(){
        this.height = 1.0;
    }
    public Cylinder(double radius){
        super(radius);
    }
    public Cylinder(double radius, double height){
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return this.height;
    }

    @Override
    public double getArea()
    {
        return this.getRadius()*this.getRadius()*3.14*this.height;
    }

}
