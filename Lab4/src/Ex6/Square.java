package Ex6;

public class Square extends Rectangle {

    public Square() {
    }

    public Square(double side) {
        super(side, side);
        side = side;
    }

    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);
        side  = side;
    }

    public double getSide() {
        return super.getLength();
    }

    public void setSide(double side) {
        this.setLenght(side);
        this.setWidth(side);
    }

    public void setWidth(double side) {
        super.setWidth(side);
    }

    public void setLenght(double side) {
        super.setLength(side);
    }
    public String toString(){
        return "A square with side = "+this.getSide()+"which is a subclass of "+super.toString();
    }
}



