package Ex6;

public class TestShapes {
    public static void main(String[] args) {
        Shape s = new Shape("pink",true);
        System.out.println(s.toString());

        Circle c = new Circle(4,"magenta",true);
        System.out.println(c.toString());
        System.out.println("Area = "+c.getArea());
        System.out.println("Perimeter = "+c.getPerimeter());
        System.out.println("Is filled = "+c.isFilled());

        Rectangle r = new Rectangle(2,3,"black",false);
        System.out.println(r.toString());
        System.out.println("Width = "+r.getWidth());
        r.setWidth(5);
        System.out.println("New width = "+r.getWidth());
        System.out.println("Area = "+r.getArea());
        System.out.println("Is filled = "+r.isFilled());

        Square sq = new Square(7,"neon",false);
        sq.setSide(3);
        System.out.println(sq.toString());
        System.out.println("Is filled = "+sq.isFilled());
        System.out.println("Perimeter = "+sq.getPerimeter());


    }
}

