package com.company.ex7;
import java.util.Random;
import java.util.Scanner;
public class Exercitiul7 {
    public static void main(String[] args){
        Random number = new Random();
        int numberToGuess=number.nextInt(100);
        Scanner input = new Scanner (System.in);
        boolean win = false;

        int numberOfTries = 0;
        int guess;

        while (win == false){
            System.out.println("Guess a number between 1 and 100: ");
            guess = input.nextInt();
            numberOfTries ++;

            if (guess == numberToGuess){
                win = true;
            }

            if (guess > 100 || guess < 0){
                System.out.println("Invalid number!");
            }

            else if (guess < numberToGuess){
                System.out.println("Wrong, the number is too low! Guess again.");
            }
            else if (guess > numberToGuess){
                System.out.println("Wrong, the number is too high! Guess again.");
            }

        }

        System.out.println("\n" + "Game Over! " + "\n" + "You guessed correctly.");
        System.out.println("The answer was: " + numberToGuess);
        System.out.println(numberOfTries + " tries.");

    }
}
