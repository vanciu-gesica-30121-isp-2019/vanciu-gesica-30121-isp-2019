package com.company.ex6;

import java.util.Scanner;

public class Exercitiul6b {
    public static void main(String[] args)
    { System.out.print("Non-recursiv");
        int n, mul = 1;
        Scanner s = new Scanner(System.in);
        n = s.nextInt();
        for(int i = 1; i <= n; i++)
        {
            mul = mul * i;
        }
        System.out.println("Factorial of "+n+" :"+mul);
    }
}
