package com.company.ex6;
import java.util.Scanner;
public class Exercitiul6a {
    public static void main(String[] args) {
        System.out.println("Recursiv ");
        int num = 3;
        long factorial = multiplyNumbers(num);
        System.out.println("Factorial of " + num + " = " + factorial);
    }
    public static long multiplyNumbers(int num)
    {
        if (num >= 1)
            return num * multiplyNumbers(num - 1);
        else
            return 1;
    }
}
