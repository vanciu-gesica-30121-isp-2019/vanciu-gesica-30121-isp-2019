package com.company.ex3;

import java.util.Scanner;

public class Exercitiul3 {
    public static void main(String[] args) {

        int low = 15, high = 60, k=0;

        while (low < high) {
            boolean flag = false;

            for(int i = 2; i <= low/2; ++i) {
                // condition for nonprime number
                if(low % i == 0) {
                    flag = true;
                    break;
                }
            }

            if (!flag) {
                System.out.print(" " + low + " ");
                k++;
            }

            ++low;
        }
        System.out.print("Contor " + k);
    }
}
