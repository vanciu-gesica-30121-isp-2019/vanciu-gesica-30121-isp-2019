package com.company.ex1;

import java.util.Scanner;

public class Exercitiul1{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter Two Number : ");
        int x = in.nextInt();
        int y = in.nextInt();
        int max;
        if (x > y) {
            max = x;
        } else {
            max = y;
        }

        System.out.print("Max is " + max);
    }
}