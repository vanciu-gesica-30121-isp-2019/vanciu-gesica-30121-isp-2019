package Ex3;

import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    private static int count = 0;


    public void control() {
        TemperatureSensor tss = new TemperatureSensor();
        LightSensor lss = new LightSensor();
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {


            public void run() {
                count++;
                if (count <= 19)
                    System.out.println(count + " )Temparature value: " + tss.readValue() + " and light value " + lss.readValue());
                else {
                    System.out.println("stop");
                    timer.cancel();
                }
                System.out.println("au fost citite  "+count+" valori");

            }

        };
        timer.schedule(task, 0, 1000);

    }


    public static void main(String[] args) {

        Controller Controller = new Controller();
        Controller.control();
    }
}
