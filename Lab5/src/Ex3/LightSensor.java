package Ex3;

import java.util.Random;

public class LightSensor extends Sensor{
    @Override
    public int readValue() {
        Random r = new Random();
        return r.nextInt(100);
    }
    @Override
    public String getLocation() {
        return null;
    }
}
