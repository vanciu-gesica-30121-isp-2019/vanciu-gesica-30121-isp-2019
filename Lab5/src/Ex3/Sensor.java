package Ex3;

abstract class Sensor {
    private String location;
    public abstract int readValue();
    public abstract String getLocation();

}
