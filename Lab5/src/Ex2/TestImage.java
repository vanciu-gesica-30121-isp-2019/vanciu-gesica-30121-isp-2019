package Ex2;

public class TestImage {
    public static void main(String[] args) {
        RealImage ri = new RealImage("Pisica cu pui");
        ri.display();
//        ProxyImage pi = new ProxyImage("Car cu boi");
//        pi.display();
        RotatedImage rri = new RotatedImage("Photo");
        rri.display();

        ProxyImage pi1 = new ProxyImage("Ghiveci cu flori",1);
        pi1.display();
        ProxyImage pi2 = new ProxyImage("Nu stiu ce sa mai scriu",2);
        pi2.display();
    }
}
