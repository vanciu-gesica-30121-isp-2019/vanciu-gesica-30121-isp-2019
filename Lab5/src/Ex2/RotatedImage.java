package Ex2;

public class RotatedImage implements Image {

    private String fileName;

    public RotatedImage(String fileName) {
        this.fileName = fileName;
        loadFromDisk(fileName);
    }


    @Override
    public void display() {
        System.out.println("Display rotated image " + fileName);
    }

    private void loadFromDisk(String fileName) {
        System.out.println("Loading rotated image " + fileName);
    }
}
