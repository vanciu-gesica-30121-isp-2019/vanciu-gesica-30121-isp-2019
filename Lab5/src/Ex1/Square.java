package Ex1;

public class Square extends Rectangle  {
    double side;

    public Square() {
    }

    public Square(double side) {
        this.side = side;
    }

    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public void setWidth(double side) {
        super.width = side;
    }

    public void setLength(double side) {
        super.length = side;
    }
    public void tooString(){
        System.out.print("A Square with side = "+this.width+" which is a subclass of Rectangle which is a subclass of ");
        if(this.filled==true)
            System.out.println("A Shape with color of " + this.color + " and filled");
        if(this.filled==false)
            System.out.println("A Shape with color of " + this.color + " and NOT filled");
    }
}
