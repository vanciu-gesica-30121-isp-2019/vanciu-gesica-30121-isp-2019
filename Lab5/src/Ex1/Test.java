package Ex1;

public class Test {
    public static void main(String[] args) {
        Circle c1 = new Circle(10.0, "blue", true);
        Circle c2 = new Circle(5.0, "yellow", false);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        c1.tooString();
        c2.tooString();

        System.out.println();

        Rectangle r1 = new Rectangle(10.0, 20.0, "red", true);
        System.out.println(r1.getArea());
        System.out.println(r1.getPerimeter());
        r1.tooString();

        System.out.println();

        Square s1 = new Square(5.0, "magenda", false);
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());
        s1.tooString();

        System.out.println();

        Square s2 = new Square();
        s2.setLength(1.0);
        s2.setWidth(1.0);
        System.out.println(s2.getArea());
        System.out.println(s2.getPerimeter());
    }
}
