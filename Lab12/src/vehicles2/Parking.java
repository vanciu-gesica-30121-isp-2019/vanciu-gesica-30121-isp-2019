package vehicles2;

import vehicles.Vehicle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Parking {
    ArrayList<Vehicle> a = new ArrayList ();

    public void parkVehicle (Vehicle e) {
        a.add (e);

    }

    /**
     * Sort vehicles by length.
     */
    public void sortByWeight () {
        Collections.sort (a, new Comparator<Vehicle>() {
            @Override
            public int compare (Vehicle o1, Vehicle o2) {
                return o1.getWeight ()-o2.getWeight ();
            }
        });
    }

    public Vehicle get (int index) {
        if(a.size ()>index)
            return a.get (index);
        else
            return null;
    }

}
