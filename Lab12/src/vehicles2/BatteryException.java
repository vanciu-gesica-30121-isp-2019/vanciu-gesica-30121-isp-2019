package vehicles2;

public class BatteryException extends Exception {

    BatteryException(String msg){
        super(msg);
    }

}