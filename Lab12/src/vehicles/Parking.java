package vehicles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Parking {

    ArrayList<Vehicle> parkedVehicles = new ArrayList<> ();

    public void parkVehicle (Vehicle e) {
        parkedVehicles.add (e);
    }


    public void sortByWeight () {
        Collections.sort (parkedVehicles, new Comparator<Vehicle>() {
            @Override
            public int compare (Vehicle o1, Vehicle o2) {
                return o1.getWeight ()-o2.getWeight ();
            }
        });

    }

    public Vehicle get (int index) {
        if(parkedVehicles.size ()>index)
            return parkedVehicles.get (index);
        else
            return null;
    }

}
