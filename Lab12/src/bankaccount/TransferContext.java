package bankaccount;

public class TransferContext {
    int balance1 = 0;
    int balance2 = 0;

    public void transfer (BankAccount sender, BankAccount receive, int ammount) {
        balance1 = sender.getBalance () - ammount;
        balance2 = receive.getBalance () + ammount;
        sender.setBalance (balance1);
        receive.setBalance (balance2);

    }


}

