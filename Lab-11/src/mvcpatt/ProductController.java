package mvcpatt;

public class ProductController {

    private Product model;
    private ProductView view;

    public ProductController(Product model, ProductView view) {
        this.model = model;
        this.view = view;
    }

    public String getProductName() {
        return model.getName();
    }

    public int getProductPrice() {
        return model.getPrice();
    }

    public int getProductQty() {
        return model.getQuantity();
    }

    public void setProductName(String name){
        model.setName(name);
    }

    public void setProductPrice(int price){
        model.setPrice(price);
    }

    public void setProductQty(int qty){
        model.setQuantity(qty);
    }

    public void updateView() {
        view.printProductDetails(model.getName(), model.getPrice(), model.getQuantity());
    }
}
