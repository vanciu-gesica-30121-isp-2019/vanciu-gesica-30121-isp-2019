package mvcpatt;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class MVCPatternTest extends JFrame {

    static List<ProductController> controller = new ArrayList<ProductController>();

    public static void addProduct(){
        Product product = new Product();
        ProductView view = new ProductView();
        product.setName("motorina");
        product.setQuantity(10);
        product.setPrice(100);
        controller.add(new ProductController(product,view));
    }

    public static void removeProduct(String nume){
        List<ProductController> ControllersRemoved = new ArrayList<ProductController>();
        for (ProductController cont:
                controller) {
            if(cont.getProductName().equals(nume)==false){
                ControllersRemoved.add(cont);
            }
        }
        controller.removeAll(controller);
        controller = ControllersRemoved;
    }

    public static void changeProductQty(String nume,int qty){
        for (ProductController cont:
                controller) {
            if(cont.getProductName().equals(nume)){
                cont.setProductQty(qty);
            }
        }
    }

    public static void main(String[] args) {
        for(int i = 0; i < 3; i++) addProduct();

        for (ProductController cont:
                controller) {
            cont.updateView();
        }

        removeProduct("motorina");

        System.out.println("removing motorina");
        for (ProductController cont:
                controller) {
            cont.updateView();
        }
        System.out.println("motorina removed");

        for(int i = 0; i < 3; i++) addProduct();
        for (ProductController cont:
                controller) {
            cont.updateView();
        }


        System.out.println("Setting qty");
        changeProductQty("motorina",100000);
        for (ProductController cont:
                controller) {
            cont.updateView();
        }
    }

}

